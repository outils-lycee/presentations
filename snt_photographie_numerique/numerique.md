<!-- .slide: data-auto-animate -->

## Numérique

Numériser

---
<!-- .slide: data-auto-animate -->

## Numérique

Convertir en nombre

---
<!-- .slide: data-auto-animate -->

## Numérique

Convertir **UNE GRANDEUR PHYSIQUE** en nombre

---
<!-- .slide: data-auto-animate -->

## Numérique

Convertir **UNE GRANDEUR PHYSIQUE** en nombre

Grâce à l'électricité