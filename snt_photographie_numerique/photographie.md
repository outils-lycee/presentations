<!-- .slide: data-auto-animate -->

# La photographie

---

<!-- .slide: data-auto-animate -->

## La photographie

Du grec 

---

<!-- .slide: data-auto-animate -->

## La photographie

Du grec 

 - photos (lumière)
 - grapho (écrire)

---

<!-- .slide: data-auto-animate -->

## La photographie

Du grec 

 - photos (lumière)
 - grapho (écrire)

Écrire avec la lumière

---

<!-- .slide: data-auto-animate data-auto-animate-restart data-background-image="snt_photographie_numerique/fond_marin.jpg" -->

## La photographie

Tout objet émet de la lumière

---

<!-- .slide: data-auto-animate data-background-image="snt_photographie_numerique/fond_marin.jpg" -->

## La photographie

Tout objet émet de la lumière

C'est ce qui permet de le voir

---

<!-- .slide: data-auto-animate data-auto-animate-restart -->

## La photographie

L'appareil photographique fonctionne comme l'œil

---

<!-- .slide: data-auto-animate -->

## La photographie

![Capteur](snt_photographie_numerique/Oeil_APN.jpg)
