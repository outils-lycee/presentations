## Principe

![capteur](snt_photographie_numerique/capteur.png)

---

## Principe

Lumière - photosite - pixel

---

## Principe

Dématriçage (vert)

![vert](snt_photographie_numerique/Vert.jpg)

---

## Principe

Dématriçage (rouge)

![rouge](snt_photographie_numerique/Rouge.jpg)

---

## Principe

### Définition de l'image

Nombre de pixels de l'image

Donnée en MP ou en donnant le nombre de pixels en largeur et en hauteur (exemple : 1900 x 1200)

---

## Principe

### Résolution  

Nombre de pixels par unité de longueur. 

Exemple : 250 points par pouce

Pour une image affichée ou imprimée
