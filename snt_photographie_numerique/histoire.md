## Histoire

---

## Histoire

### Nicéphore Niépce et Daguerre

1824  
 - premières images
 - Bitume de Judée sur plaque d'argent

Temps de pose : plusieurs jours

---

<!-- .slide: data-background-image="snt_photographie_numerique/gras.jpg" -->

---

## Histoire

### Nicéphore Niépce et Daguerre

1838
 - Daguerréotype
 - iodure d'argent
 - développé au mercure

Temps de pose 30 minutes

---

## Histoire

### Hyppolyte Fizeau

1841 
 - Remplace l'iodure d'argent par du bromure d'argent
 - temps de pose en seconde
 - portraits

---

<!-- .slide: data-background-image="snt_photographie_numerique/Fizeau.jpg" -->

---

## Histoire

### Abel Niépce de Saint-Victor

1847
 - petit cousin de Nicéphore
 - plaques de verre 
 - grande précision

---

## Histoire

### Georges Eastmann 

 1888
 - invention du support souple
 - fondateur de Kodak
 - la pellicule est née !

---

<!-- .slide: data-background-image="snt_photographie_numerique/kodak.jpg" -->

---
## Histoire

### Les frères lumières

 1906 
  - procédé couleur
  - grain de fécules
  - mosaique de couleurs
  - autochromes lumières

---

<!-- .slide: data-background-image="snt_photographie_numerique/autochrome.jpg" -->

---

## Histoire 

### La couleur

 - 1935 : Kodachrome
 - 1936 : Agfacolor
 - Basé sur le bromure d'argent 
 - couches de gélatine
 - bleu, vert et rouge

---

<!-- .slide: data-background-image="snt_photographie_numerique/kodachrome.webp" -->

