# Photographie numérique

## Histoire

---

## Histoire

1969 : invention du capteur CCD (charge coupled device)

---

## Histoire

1975 : Steve Sasson chez Kodak

 - premier appareil photo numérique
 - 4kg
 - noir et blanc
 - sur cassette

---

## Histoire 

1992 : Kodak DCS 200 (1.54 MP)

![DCS200](snt_photographie_numerique/Kodak_DCS200.jpg)

---

## Histoire

2003 : 
 - Reflex numérique "grand public"
 - 6 MP
 - abordables
 - Canon EOS 300D, digital rebel
 - nikon D70

---

<!-- .slide: data-background-image="snt_photographie_numerique/eos-300d.jpg" -->

---

## Histoire

2007 : Premier iphone

---

<!-- .slide: data-background-image="snt_photographie_numerique/iphone.jpg" -->



