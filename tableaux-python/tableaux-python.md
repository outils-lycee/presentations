# Les tableaux

---

<!-- .slide: data-auto-animate -->

### Une variable

![affectation](tableaux-python/boite.png)

Une boîte

--

<!-- .slide: data-auto-animate -->

### Une variable

![affectation](tableaux-python/boite_valeur.png)

Avec du contenu

--

<!-- .slide: data-auto-animate -->

### Une variable

![affectation](tableaux-python/affectation.png)

Avec un nom

(affectation)

---

## Problème 

Et si j'ai plein de boîtes ?

![boites](tableaux-python/boxes.jpg)

--

<!-- .slide: data-auto-animate -->

### Exemple 

La température d'une salle du lycée

La température de **toutes** les salles du lycée

--

<!-- .slide: data-auto-animate -->

### Exemple 

La température d'une salle du lycée

 - une variable 

La température de **toutes** les salles du lycée

 - autant de variables que de salles ?

--

<!-- .slide: data-auto-animate -->

### Exemple

Ça fait beaucoup trop de variables différentes.

Comment les utiliser ??

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Une étagère

Quand on a beaucoup de boites, on les range dans une étagère

![etagere](tableaux-python/etagere.png)


--

<!-- .slide: data-auto-animate -->

### Une étagère

Cette étagère est elle même une boite 

![etagere](tableaux-python/etagere.png)

Une boite qui contient des boites

--

<!-- .slide: data-auto-animate -->

### Une étagère

Une boite avec un nom

![etagere](tableaux-python/etagere-etiquette.png)

monEtagere est une variable

--

<!-- .slide: data-auto-animate -->


### Une étagère


Pour retrouver les boites dedans, on les numérote

![etagere](tableaux-python/etagere-numero.png)

En partant de 0

--

<!-- .slide: data-auto-animate -->

### Une étagère

monEtagere[2] est la variable correspondant à la boite n°2

![etagere](tableaux-python/etagere-numero.png)

qui est la troisième en partant du bas

---

## En python


Pour stocker un grand nombre de variables, on utilise un tableau

C'est **une variable** qui contient d'autres variables

--

<!-- .slide: data-auto-animate -->

## En python 

Création d'un tableau vide

```python
monTableau = []
```

--

<!-- .slide: data-auto-animate -->

## En python 

Création d'un tableau contenant 10 fois 0

```python
monTableau = [0] * 10
```

(Ce que l'on trouve dans France IOI)

--

<!-- .slide: data-auto-animate -->

## En python 

Création d'un tableau contenant 10 fois 0

```python
monTableau = [0] * 10
```
![tableau-zero](tableaux-python/monTableau.png)

--

<!-- .slide: data-auto-animate -->

## En python 

Modification d'un élément

```python
monTableau = [0] * 10
monTableau[3] = 5 
```
![tableau-zero](tableaux-python/monTableau-3-5.png)

--

## En python

<!-- .slide: data-auto-animate -->

Accéder à un élément

```python
monTableau = [0] * 10
monTableau[3] = 5 
a = monTableau[3] * monTableau[3] # a vaut 25
```

--

## En python

<!-- .slide: data-auto-animate -->

Un tableau peut contenir tout type d'élément

```python
monTableau = [0] * 13
monTableau[1] = "janvier" 
```

--

## En python

<!-- .slide: data-auto-animate -->

On peut remplir le tableau à la création

```python
beatles =  ["john","paul","georges","ringo"]
```

---

## Parcourir un tableau

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

première méthode : par les indices

```python
beatles =  ["john","paul","georges","ringo"]
nbBeatles = len(beatles) # 4 
```

la fonction *len* renvoi la longueur du tableau

--

## Parcourir un tableau

<!-- .slide: data-auto-animate -->

on utilise la boucle for avec les indices des éléments

```python [4-5]
beatles =  ["john","paul","georges","ringo"]
nbBeatles = len(beatles) # 4 
print("Les Beatles étaient : ")
for loop in range(nbBeatles) :
    print(beatles[loop])
```

--

## Parcourir un tableau

<!-- .slide: data-auto-animate -->

seconde méthode : par les éléments

```python [3-4] 
beatles =  ["john","paul","georges","ringo"]
print("Les Beatles étaient : ")
for nom in beatles :
    print(nom)
```

---

## Fonction utiles

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-restart -->

Il y a tout un tas de fonction utiles sur les tableaux. 

Les tableaux en python sont des tableaux *dynamiques*

Ils peuvent changer de taille

--

## Fonction utiles

<!-- .slide: data-auto-animate -->

Pour avoir la taille d'un tableau, la fonction <code>len</code>

```python
monTableau = ["a","b","c"]
taille = len(monTableau)
```

taille vaut 3

--

## Fonction utiles

<!-- .slide: data-auto-animate -->

Pour ajouter un élément à la fin d'un tableau, <code>append</code>

```python
monTableau = ["a","b","c"]
monTableau.append("d")
```

monTableau est maintenant ["a","b","c","d"]

--

## Fonction utiles

<!-- .slide: data-auto-animate -->

Pour supprimer le dernier élément, <code>pop</code>

```python
monTableau = ["a","b","c","d"]
valeur = monTableau.pop()
```

monTableau est maintenant ["a","b","c"]

valeur vaut "d"

--

## Fonction utiles

<!-- .slide: data-auto-animate -->

<code>pop</code> permet, avec un argument, de retirer un élément défini

```python
monTableau = ["a","b","c","d"]
valeur = monTableau.pop(1)
```

monTableau est maintenant ["a","c","d"]

valeur vaut "b"

--

## Fonction utiles

<!-- .slide: data-auto-animate -->

Enfin, on peut ajouter un élément à un endroit avec <code>insert</code>

```python
monTableau = ["a","b","d","e"]
monTableau.insert(2,"c")
```

monTableau est maintenant ["a","b","c","d","e"]

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->


## Tableau par compréhension

On veut faire une opération 
sur tous les éléments d'un tableau

Par exemple, faire le carré de tous les éléments d'un tableau

--

<!-- .slide: data-auto-animate -->

## Tableau par compréhension

Avec ce que l'on connaît :

```python
T = [2,5,7,9]
T2 = []
for n in T :
    T2.append(n**2)
```

--

<!-- .slide: data-auto-animate -->

## Tableau par compréhension

Par compréhension :

```python
T = [2,5,7,9]
T2 = [ n**2 for n in T]
```

On construit le tableau des carrés des éléments de T

--

## Tableau par compréhension

On peut même rajouter des conditions :

Si on veut le carré des nombres *plus grand que 3* dans le tableau

--

<!-- .slide: data-auto-animate -->

## Tableau par compréhension

Version itérative :

```python
T = [2,5,7,9]
T2 = []
for n in T :
    if n > 3 :
        T2.append(n**2)
```

T2 vaut [25, 49, 81]

--

<!-- .slide: data-auto-animate -->

## Tableau par compréhension

Version par compréhension :

```python
T = [2,5,7,9]
T2 = [ n**2 for n in T if n > 3 ]
```

T2 vaut [25, 49, 81]



