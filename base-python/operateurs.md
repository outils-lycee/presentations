## Opérateurs

On peut effectuer des opérations sur les variables. L'ordinateur est avant tout un **calculateur**

---
<!-- .slide: data-auto-animate -->
### Opérateurs numériques

Addition

```python [1]
a = 3 + 5 
b = a - 6 
c = b * 4
d = 12 / 5 
```

---

<!-- .slide: data-auto-animate -->
### Opérateurs numériques

Soustraction

```python [2]
a = 3 + 5 
b = a - 6 
c = b * 4
d = 12 / 5 
```

---

<!-- .slide: data-auto-animate -->

### Opérateurs numériques

Multiplication

```python [3]
a = 3 + 5 
b = a - 6 
c = b * 4
d = 12 / 5 
```

---

<!-- .slide: data-auto-animate -->

### Opérateurs numériques

Division 

```python [4]
a = 3 + 5 
b = a - 6 
c = b * 4
d = 12 / 5 
```

---
<!-- .slide: data-auto-animate -->


### Opérateurs numériques entiers

Division euclidienne (entière)

```python [1]
e = 13 // 5
```

e vaut 2

---

<!-- .slide: data-auto-animate -->
### Opérateurs numériques entiers

Modulo (reste de la division euclidienne)

```python [2] 
e = 13 // 5 
f = 13 % 5
```

f vaut 3

---

<!-- .slide: data-auto-animate -->
### Autre Opérateur numérique

Puissance

```python 
g = 4 ** 3
```

4 au cube

---

### Opérateurs numériques

#### Résumé 

```python
a = 3 + 5 # somme : a vaut 8
b = a - 6 # différence : b vaut 2
c = b * 4 # produit : c vaut 8
d = 12 / 5 # quotient : d vaut 2.4
e = 13 // 5 # division euclidienne : e vaut 2
f = 13 % 5 # reste : f vaut 3
g = 4 ** 3 # puissance : g vaut 64
```

---

### Opérateurs sur les chaînes de caractères

Concaténation 

```python
salutation = "Bonjour"
personne = "M. Van Rossum"
phrase = salutation + " " + personne
```

phrase vaut "Bonjour M. Van Rossum"

---

### Opérateurs sur les chaînes de caractères

Multiplication 

```python
chaine = "abc" * 3 
```

chaine vaut "abcabcabc"

---

<!-- .slide: data-auto-animate -->
### typage

**attention** : on ne peut pas mélanger les chaines de caractère et les nombres. Voici des
exemples d'erreur

```python
a = 3
b = "4"
c = a + b # ERREUR
```

est une somme (c vaudrait 7) ou une concaténation (c vaudrait 34)

---

<!-- .slide: data-auto-animate -->
### typage

on peut convertir une chaine en  entier avec *int*

```python
a = 3
b = "4"
c = a + int(b) # c vaut 7
```

---

<!-- .slide: data-auto-animate -->
### typage

on peut convertir une chaine en  flottant avec *float*

```python
a = 3
b = "5.2"
c = a + float(b) # c vaut 8.2
```

---

<!-- .slide: data-auto-animate -->
### typage

on peut convertir en nombre en chaine de caractère avec *str*

```python
a = 3
b = "4"
c = str(a) + b # c vaut "34"
```

---

### exemple complet 

*input* acquiert une entrée utilisateur

*input* renvoie toujours une chaine

```python [1,2]
largeur  = input("Quelle est la largeur ?")
longueur = input("Quelle est la longueur ? ")
aire = float(longueur) * float(largeur)
print("L'aire est de " + str(aire))
```

---

### exemple complet 

Pour faire le produit, on convertit en nombre.

```python [3]
largeur  = input("Quelle est la largeur ?")
longueur = input("Quelle est la longueur ? ")
aire = float(longueur) * float(largeur)
print("L'aire est de " + str(aire))
```

---

### exemple complet 

Pour faire la concaténation, on convertit en chaîne de caractères.

```python [4]
largeur  = input("Quelle est la largeur ?")
longueur = input("Quelle est la longueur ? ")
aire = float(longueur) * float(largeur)
print("L'aire est de " + str(aire))
```

