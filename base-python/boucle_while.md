## Boucles non bornées

<!-- .slide: data-auto-animate -->

En scratch, on répète **jusqu'à ce que**

![Scratch](base-python/assets/scratch_while.png)

---

<!-- .slide: data-auto-animate -->
## Boucles non bornées

En python, on répète **tant que** (while)

```python
resultat = 0
while resultat < 9 :
    print("On recommence")
    resultat = passerQcm()
```

---

<!-- .slide: data-auto-animate -->
## Boucles non bornées

On initialise la condition

```python[1]
resultat = 0
while resultat < 9 :
    print("On recommence")
    resultat = passerQcm()
```

---

<!-- .slide: data-auto-animate -->
## Boucles non bornées

On vérifie la condition

```python[2]
resultat = 0
while resultat < 9 :
    print("On recommence")
    resultat = passerQcm()
```

---

<!-- .slide: data-auto-animate -->
## Boucles non bornées

On effectue une action qui peut modifier la condition

```python[3-4]
resultat = 0
while resultat < 9 :
    print("On recommence")
    resultat = passerQcm()
```

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

## Équivalence
On peut créer l'équivalent d'un *for loop in range()*

```python
for loop in range(5):
    print("Bonjour !")
```

---

<!-- .slide: data-auto-animate -->

## Équivalence

On peut créer l'équivalent d'un *for loop in range()*

```python
loop = 0
while loop < 5 :
    print("Bonjour !")
    loop = loop + 1
```