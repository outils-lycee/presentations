## Tests

En scratch

![test si alors ](base-python/assets/scratch_if.png)

ce qui se place dans l'hexagone est une condition (un booléen) <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Tests

En python, le mot clef est *if*

```python
age = int(input("Quel est votre age?"))
if age >= 18 :
    print("Vous êtes majeur.")
    print("Vous pouvez voter.")
```

Comme pour les boucles, c'est le bloc d'instruction qui est exécuté

---

<!-- .slide: data-auto-animate -->

## Tests

On peut faire avoir un comportement par défaut (si / sinon)

En scratch

![test si alors ](base-python/assets/scratch_si_sinon.png)

---

<!-- .slide: data-auto-animate -->

## Tests

On peut faire avoir un comportement par défaut (si / sinon)

En python, le mot clef est *else*

```python
if age >= 18 :
    print("Vous êtes majeur.")
    print("Vous pouvez voter.")
else :
    print("Vous êtes mineur, vous ne pouvez pas voter.")
```

---

<!-- .slide: data-auto-animate -->

## Tests

On peut aussi avoir plusieurs tests successifs 

Le mot cles est *elif* (contraction de else if)

```python
if age < 16 :
    print("Trop jeune, mineur.")
elif age < 65 :
    print("Bon pour le service !")
else :
    print("Vrop vieux, retraite !")
```

NB : un seul des trois cas sera traité.

---

## Comparaisons

Les conditions sont souvent le résultat de comparaisons

---

<!-- .slide: data-auto-animate -->

## Comparaisons

inférieur

```python[1]
a < b # vrai si a est plus petit que b
```

---

<!-- .slide: data-auto-animate -->

## Comparaisons

supérieur

```python[2]
a < b # vrai si a est plus petit que b
a > c # vrai si a est plus grand que b
```

---

<!-- .slide: data-auto-animate -->

## Comparaisons

inférieur ou égal

```python[3]
a < b # vrai si a est plus petit que b
a > c # vrai si a est plus grand que b
a <= b # vrai si a est plus petit ou égal à b
```

---

<!-- .slide: data-auto-animate -->

## Comparaisons

supérieur ou égal

```python[4]
a < b # vrai si a est plus petit que b
a > c # vrai si a est plus grand que b
a <= b # vrai si a est plus petit ou égal à b
a >= b # vrai si a est plus grand ou égal à b
```

---

<!-- .slide: data-auto-animate -->

## Comparaisons

égal

```python[5]
a < b # vrai si a est plus petit que b
a > c # vrai si a est plus grand que b
a <= b # vrai si a est plus petit ou égal à b
a >= b # vrai si a est plus grand ou égal à b
a == c # vrai si a est égal à b 
```

*Marche pour tout type de variables, pas seulement les nombres*

---

<!-- .slide: data-auto-animate -->

## Comparaisons

différent

```python[6]
a < b # vrai si a est plus petit que b
a > c # vrai si a est plus grand que b
a <= b # vrai si a est plus petit ou égal à b
a >= b # vrai si a est plus grand ou égal à b
a == c # vrai si a est égal à b 
a != c # vrai si a est différent de b

```

*Marche pour tout type de variables, pas seulement les nombres*

---

### Booléens

Le résultat d'une comparaison est un booléen

```python
condition = age < 16
print(type(condition))
```

Va afficher *\<class 'bool'\>*

---

<!-- .slide: data-auto-animate -->

### Booléens

On peut combiner plusieurs conditions grâce aux opérateurs 

 - *and* (et)

```python 
if poids <= 10 and volume <= 1.5 :
    print("Votre colis a le bon gabarit") 
```

---

<!-- .slide: data-auto-animate -->

### Booléens

On peut combiner plusieurs conditions grâce aux opérateurs 

 - *and* (et)
 - *or* (ou)

```python [3,4]
if poids <= 10 and volume <= 1.5 :
    print("Votre colis a le bon gabarit") 
if taille <= 150 or taille >= 197 :
    print("Taille incompatible avec le cockpit")
```

---

<!-- .slide: data-auto-animate -->

### Booléens

Ou faire une négation avec *not*

```python [5-6]
if poids <= 10 and volume <= 1.5 :
    print("Votre colis a le bon gabarit") 
if taille <= 150 or taille >= 197 :
    print("Taille incompatible avec le cockpit")
if not motDePasse == "superSecret" :
    print("Mauvais mot de passe")    
```

---

<!-- .slide: data-auto-animate -->

### Booléens

Ou combiner tout cela

```python [7-8]
if poids <= 10 and volume <= 1.5 :
    print("Votre colis a le bon gabarit") 
if taille <= 150 or taille >= 197 :
    print("Taille incompatible avec le cockpit")
if not motDePasse == "superSecret" :
    print("Mauvais mot de passe")    
if not ( a < b and b < c ) or a > c :
    print("ça ne va pas le faire ...") 
```