## Les fonctions

En scratch, les blocs personnalisés

![Scratch](base-python/assets/scratch_fonction.png)

---

<!-- .slide: data-auto-animate -->

## Les fonctions

En python, on définit une fonction en 
utilisant le mot clef *def*

```python[1]
def maFonction(x):
    resultat = x ** 2 - 2 * x + 4
    return resultat
```

---

<!-- .slide: data-auto-animate -->

## Les fonctions

Une fonction peut prendre des paramètres, comme ici le *x*

```python[1]
def maFonction(x):
    resultat = x ** 2 - 2 * x + 4
    return resultat
```

---

<!-- .slide: data-auto-animate -->

## Les fonctions

Dans le corps de la fonction, on effectue les opérations

```python[2]
def maFonction(x):
    resultat = x ** 2 - 2 * x + 4
    return resultat
```

---

<!-- .slide: data-auto-animate -->

## Les fonctions

Et enfin, on retourne le résultat

```python[3]
def maFonction(x):
    resultat = x ** 2 - 2 * x + 4
    return resultat
```

---

<!-- .slide: data-auto-animate -->

## Les fonctions

Un fonction peut prendre plusieurs variables

```python
def distance(x1,y1,x2,y2):
    d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return d
```

---

<!-- .slide: data-auto-animate -->

## Les fonctions

Ou n'en prendre aucune

```python
def aleatoire():
    tirage1 = random.random()
    tirage2 = random.random() * 10
    return tirage1, tirage2
```

---

### Exemple complet

```python
def peutVoter(age,nationalite):
    vote = True
    if age < 18 :
        vote = False
    if nationalite != "français" and nationalite != "francais" :
        vote = False
    return vote

a = int(input("Quel est votre age ? "))
n = int(input("Quelle est votre nationalité ? "))

if peutVoter(a,n):
    print("Vous avez le droit de vote")
else :
    print("Désolé, vous n'avez pas le droit de vote") 
```