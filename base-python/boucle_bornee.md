## Boucles bornées

En scratch 

![Scratch](base-python/assets/scratch_for.png)

---

<!-- .slide: data-auto-animate -->
## Boucles bornées

```python
for loop in range(5):
    print("Bonjour !")
```

Affiche 5 fois "Bonjour !"

---


<!-- .slide: data-auto-animate -->
## Boucles bornées

Tout ce qui est décalé après le *for* est répété

```python
for loop in range(5):
    print("Bonjour !")
    print("Ça va?")
```

Affiche 5 fois "Bonjour !" et "Ça va"

---

<!-- .slide: data-auto-animate -->
## Boucles bornées

Le bloc se termine quand le décalage s'arrête

```python
for loop in range(5):
    print("Bonjour !")
print("Ça va?")
```

Affiche 5 fois "Bonjour !" mais une seule fois "Ça va ?" 

(qui n'est pas **dans** le bloc d'instruction de la boucle)

---

<!-- .slide: data-auto-animate -->
## Boucles bornées

```python
for loop in range(5):
    print("Bonjour !")
```

La fonction *range* crée l'équivalent d'une liste de nombre

---

<!-- .slide: data-auto-animate -->
## Boucles bornées

*loop* prend les valeurs fournies par  *range(5)*

```python
for loop in range(5):
    print(loop)
```

affichera 0,1,2,3 et 4  (5 valeurs de 0 à 4)

---

<!-- .slide: data-auto-animate -->

## Boucles bornées

*range* peut aussi prendre deux paramètres : 
 début et fin (exclu)

```python
for loop in range(1,6):
    print(loop)
```

affichera 1,2,3,4 et 5  (5 valeurs de 1 à 5)

---

<!-- .slide: data-auto-animate -->

## Boucles bornées

*range* peut prendre trois paramètres : 
 début, fin (exclu) et le pas

```python
for loop in range(20,-1,-4):
    print(loop)
```

affichera 20,16,12,8,4 et 0  (de 20 à -1 en retirant 4 à chaque fois)

---

## Exemple complet

On veut afficher un triangle d'étoiles de hauteur 5.

```shell
*
**
***
****
*****
```

---

<!-- .slide: data-auto-animate -->

### Exemple complet

Version avec boucles imbriquées

```python
for i in range(1,6):
    for j in range(i):
        print("*",end="")
    print()
```

---

<!-- .slide: data-auto-animate -->

### Exemple complet

Version avec une seule boucle

```python
for i in range(1,6):
    print("*" * i)
```
