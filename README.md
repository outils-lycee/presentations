Ensemble de présentation interactives se déclinant en page web simples.

Un fichier template.html est disponible pour faire de nouvelles présentations

Chaque présentation consiste en 
 - un fichier html
 - un répertoire contenant les slides au format md et les éventuels assets (images, css, etc)

Toutes les pages html de ce dépot se retrouvent sur gitlab pages à l'adresse 
[https://outils-lycee.gitlab.io/presentations/](https://outils-lycee.gitlab.io/presentations/)
