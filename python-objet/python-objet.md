# Programmation objet

## Principes fondamentaux

---

<!-- .slide: data-auto-animate -->

### Tout est objet

- En Python, tout est objet.

o-

<!-- .slide: data-auto-animate -->

### Tout est objet

- En Python, tout est objet.
- Exemples :

  - Les listes
  - les chaînes de caractères

o-

<!-- .slide: data-auto-animate -->

### Tout est objet

- En Python, tout est objet.
- Exemple en python

```python
ma_liste = [1, 2, 3]
ma_liste.append(4) 
```

o-

<!-- .slide: data-auto-animate -->

### Tout est objet

- En Python, tout est objet.
- Exemple en python

```python
ma_liste = [1, 2, 3]
ma_liste.append(4) 
# `append` : méthode de la classe `list`
```

---
<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Encapsulation

- Un Pilier de la POO
- Regroupement des données (attributs) et des méthodes au sein d'une classe

o-

<!-- .slide: data-auto-animate -->

### Encapsulation

#### Pourquoi C'est Important ?

- Protège l'état interne de l'objet.

o-

<!-- .slide: data-auto-animate -->

### Encapsulation

#### Pourquoi C'est Important ?

- Protège l'état interne de l'objet.
- Empêche les modifications extérieures non autorisées.

o-

<!-- .slide: data-auto-animate -->

### Encapsulation

#### Pourquoi C'est Important ?

- Protège l'état interne de l'objet.
- Empêche les modifications extérieures non autorisées.
- Simplifie l'utilisation et la maintenance du code.

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Notion de classe

- Une classe est un **modèle** ou un plan pour créer des objets.

o-

<!-- .slide: data-auto-animate -->

### Notion de classe

- Une classe est un **modèle** ou un plan pour créer des objets.
- Elle définit :
    - les attributs de l'objet
    - les méthodes de l'objet.

o-

<!-- .slide: data-auto-animate -->

### Notion de classe

- Exemple en Python:

```python
class Voiture:
    pass
```

o-

<!-- .slide: data-auto-animate -->

### Notion de classe

- Exemple en Python:

```python
class Voiture:
    pass

# Une instance de la classe voiture
ma_voiture = Voiture()
```

o-

<!-- .slide: data-auto-animate -->

### Notion de classe

Si on considère que la classe est un modèle, utilisé dans une usine, une instance de la classe
est le produit concret qui sort de l'usine. C'est lui que le programme manipule.

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

#### Attribut

- variable associée aux objets

o-

<!-- .slide: data-auto-animate -->

#### Attribut

- variable associée aux objets

```python
class Voiture:
    self.couleur = 'noire'
```

o-

<!-- .slide: data-auto-animate -->

#### Attribut

- variable associée aux objets

```python
class Voiture:
    self.couleur = 'noire'
```

“Les gens peuvent choisir n’importe quelle couleur pour la Ford T, du moment que c’est noir.”
**Henry FORD**

---
<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Méthode

- Fonction définie à l'intérieur d'une classe.
- Décrit un comportement ou une action que les instances de la classe peuvent réaliser.

o-

<!-- .slide: data-auto-animate -->

### Méthode

#### Exemple

```python
class Voiture:
    self.voiture = 'noire'

    def afficher_couleur(self):
        print("La couleur de la voiture est " + self.couleur)
```

o-

<!-- .slide: data-auto-animate -->

### Méthode

#### Exemple

```python
class Voiture:
    self.voiture = 'noire'

    def afficher_couleur(self):
        print("La couleur de la voiture est " + self.couleur)

ma_voiture = Voiture()
ma_voiture.afficher_couleur()

```

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Constructeur et paramètres

```python
class Voiture:
    def __init__(self,c):
        self.vitesse = 0  
        self.couleur = c

    def afficher_couleur(self):
        print("La couleur de la voiture est " + self.couleur)

    def accelerer(self, valeur):
        self.vitesse += valeur  
```

o-

<!-- .slide: data-auto-animate -->

### Constructeur et paramètres

```python
class Voiture:
    def __init__(self,c):
        self.vitesse = 0  
        self.couleur = c

    def afficher_couleur(self):
        print("La couleur de la voiture est " + self.couleur)

    def accelerer(self, valeur):
        self.vitesse += valeur  

ma_voiture = Voiture('rouge')
ma_voiture.accelerer(3)
```

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Conventions

- attributs privés
- ne devraient pas être utilisés en dehors
- commencent par `--`

o-

<!-- .slide: data-auto-animate -->

### Conventions

#### Exemple

```python
class Point:
    def __init__(self,x,y):
        self.__abscisse = x
        self.__ordonnee = y
```

o-

<!-- .slide: data-auto-animate -->

### Conventions

- attributs privés
- ne devraient pas être utilisés en dehors
- commencent par `--`
- utilisation de *getters* et de *setters*

o-

<!-- .slide: data-auto-animate -->

### Conventions

#### Exemple

```python
class Point:
    def __init__(self,x,y):
        self.__abscisse = x
        self.__ordonnee = y

    def set_abscisse(self,x):
        self.__abscisse = x

    def get_abscisse(self):
        return self.__abscisse
```

o-

<!-- .slide: data-auto-animate -->

### Conventions

- permet de changer l'implémentation
  - variables séparées
  - tuple `(x,y)`
  - liste `[x,y]`
  - dictionnaire `{'x':x,'y':y}`
  - etc.

o-

<!-- .slide: data-auto-animate -->

### Conventions

- méthodes privés
- ne devraient pas être utilisés en dehors
- commencent par `--`

o-

<!-- .slide: data-auto-animate -->

### Conventions

#### Exemple

```python
class Voiture:
    def __methode_privee(self):
        # fait un travail interne 
        # à l'instance de l'objet
        # que les utilisateurs ne connaissant pas
```

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Attributs de classe

o-

<!-- .slide: data-auto-animate -->

### Attributs de classe

- attribut commun à toutes les instances
- au niveau de la classe

o-

<!-- .slide: data-auto-animate -->

### Attributs de classe

- pas de `self`
- nom de la classe devant

```python
class Voiture:
    # Compteur lié à la classe
    nombre_total_de_voitures_fabriquées = 0

    def __init__(self, marque, couleur):
        print("Construction d'une", marque, couleur)
        Voiture.nombre_total_de_voitures_fabriquées += 1

```

---

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

## Autres notions

o-

<!-- .slide: data-auto-animate -->

## Autres notions

- méthodes de classe
- méthodes statiques

o-

<!-- .slide: data-auto-animate -->

## Autres notions

- méthodes de classe
- méthodes statiques
- héritage

o-

<!-- .slide: data-auto-animate -->

## Autres notions

- méthodes de classe
- méthodes statiques
- héritage
- polymorphisme
- etc.
