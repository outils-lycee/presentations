# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2021-11-07
### Added
 - template for building a presentation
 - presentation for "base du langage python"
 - presentation for "programme de seconde"
 - simple css style for light presentations  
 - Readme.md
 - Licence CC-NC-BY-SA

### Changed
 - nothing

### Removed
 - nothing
