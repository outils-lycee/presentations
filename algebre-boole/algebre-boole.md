# L'algèbre de boole

Introduction à l'algèbre de Boole et son utilisation en informatique

---

<!-- .slide: data-auto-animate -->

### Georges BOOLE

![Georges Boole](algebre-boole/Boole.jpg)

--

<!-- .slide: data-auto-animate -->

### Georges BOOLE

Mathématicien britannique (1815-1864)

--

<!-- .slide: data-auto-animate -->

### Georges BOOLE

Mathématicien britannique (1815-1864)

Veut rendre la logique calculable

--

<!-- .slide: data-auto-animate -->

### Georges BOOLE

Mathématicien britannique (1815-1864)

Veut rendre la logique calculable

Crée l'algèbre de Boole

--

<!-- .slide: data-auto-animate -->

### Georges BOOLE

Mathématicien britannique (1815-1864)

Veut rendre la logique calculable

Crée l'algèbre de Boole

Sert de fondement à l'informatique et aux systèmes électroniques

---

### Algèbre de Boole

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

Utilise l'ensemble B = { 0 ; 1 }

--

### Algèbre de Boole

<!-- .slide: data-auto-animate -->

Utilise l'ensemble B = { 0 ; 1 }

On associe 0 à faux et 1 à vrai

--

### Algèbre de Boole 

<!-- .slide: data-auto-animate -->

Utilise l'ensemble B = { 0 ; 1 }

On associe 0 à faux et 1 à vrai

Les éléments de B sont les booléens

--

### Algèbre de Boole 

<!-- .slide: data-auto-animate -->

Utilise l'ensemble B = { 0 ; 1 }

On associe 0 à faux et 1 à vrai

Les éléments de B sont les booléens

Deux opérations : **ET** et **OU**

Un opérateur : **NON**

---

### Tables de vérité

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

B = { 0 ; 1 }

Deux opérations : **ET** et **OU**

Un opérateur : **NON**

On définit ces opérations grâce à des *tables de vérité*

--

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Tables de vérité : NON

**NON** a est vrai est équivalent à 

a est faux 

(et réciproquement)

--

<!-- .slide: data-auto-animate -->

### Tables de vérité : NON

<table  style="width:auto;text-align: center;">
        <thead>
            <tr>
                <th colspan="2"><b>NON</b></th>
            </tr>
            <tr>
                <th>P</th>
                <th>non P</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0</td>
                <td>1</td>
            </tr>
            <tr>
                <td>1</td>
                <td>0</td>
            </tr>
        </tbody>
    </table>

--
<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Tables de vérité : ET

a **ET** b est vrai est équivalent à 

"a et b sont tous les deux vrais"

--

<!-- .slide: data-auto-animate -->

### Tables de vérité : ET

<table  style="width:auto;text-align: center;">
        <thead>
            <tr>
                <th colspan="3"><b>ET</b></th>
            </tr>
            <tr>
                <th>P</th>
                <th>Q</th>
                <th>P et Q</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0</td>
                <td>0</td>
                <td>0</td>
            </tr>
            <tr>
                <td>0</td>
                <td>1</td>
                <td>0</td>
            </tr>
            <tr>
                <td>1</td>
                <td>0</td>
                <td>0</td>
            </tr>
            <tr>
                <td>1</td>
                <td>1</td>
                <td>1</td>
            </tr>
        </tbody>
    </table>

--

<!-- .slide: data-auto-animate-restart -->
<!-- .slide: data-auto-animate -->

### Table de vérité : OU

a **OU** b est équivalent 

à "au moins l'un des deux est vrai"

(ou inclusif)

--

<!-- .slide: data-auto-animate -->

### Table de vérité : OU

<table  style="width:auto;text-align: center;">
        <thead>
            <tr>
                <th colspan="3">OU</th>
            </tr>
            <tr>
                <th>P</th>
                <th>Q</th>
                <th>P OU Q</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0</td>
                <td>0</td>
                <td>0</td>
            </tr>
            <tr>
                <td>0</td>
                <td>1</td>
                <td>1</td>
            </tr>
            <tr>
                <td>1</td>
                <td>0</td>
                <td>1</td>
            </tr>
            <tr>
                <td>1</td>
                <td>1</td>
                <td>1</td>
            </tr>
        </tbody>
    </table>

---

## Notations

<table>
    <thead>
        <tr>
            <th></th>
            <th>0</th>
            <th>1</th>
            <th>a et b</th>
            <th>a ou b</th>
            <th>non a</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>logique formelle</td>
            <td>&#8869;</td>
            <td>&#8868;</td>
            <td>a &#8743; b</td>
            <td>a &#8744; b</td>
            <td> &#172; a </td>
        </tr>
        <tr>
            <td>Informatique</td>
            <td>False</td>
            <td>True</td>
            <td> a & b</td>
            <td>a | b</td>
            <td> ! a </td>
        </tr>
        <tr>
            <td>algébrique</td>
            <td>0</td>
            <td>1</td>
            <td>a.b</td>
            <td>a+b</td>
            <td> &amacr; </td>
        </tr>
    </tbody>
</table>

---

### Opérateurs logiques en Python

<table>
    <thead>
        <tr>
            <th></th>
            <th>a et b</th>
            <th>a ou b</th>
            <th>non a</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Bitwise (bit à bit)</td>
            <td> a & b</td>
            <td>a | b</td>
            <td> ~ a </td>
        </tr>
        <tr>
            <td>Booléen</td>
            <td>a and b</td>
            <td>a or b</td>
            <td> not a </td>
        </tr>
    </tbody>
</table>


---

<!-- .slide :data-auto-animate-restart>
<!-- .slide: data-auto-animate -->

## Propriétés

--

<!-- .slide: data-auto-animate -->

## Propriétés

**complémentarité** 

a ou (non a) = 1 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
a et (non a) = 0 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
non (non a) = a

--

<!-- .slide: data-auto-animate -->

## Propriétés

**Commutativité**

a ou b = b ou a 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
a et b = b et a

--

<!-- .slide: data-auto-animate -->

## Propriétés

**associativité**

(a ou b) ou c = a ou (b ou c) = a ou b ou c

(a et b) et c = a et (b et c) = a et b et c

--

<!-- .slide: data-auto-animate -->

## Propriétés

**Distributivité**

a et (b ou c) = (a et b) ou (a et c) 

a ou ( b et c) = (a ou b) et (a ou c)

