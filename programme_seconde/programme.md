----
## Programme de Mathématiques
----

Partie algorithmique et programmation du 
[**programme de seconde**](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/95/7/spe631_annexe_1062957.pdf)

---

### Utiliser les variables et les instructions élémentaires 

 - Variables informatiques de type entier, booléen, flottant, chaîne de caractères. 
 - Affectation (notée ← en langage naturel). 
 - Séquence d’instructions. 
 - Instruction conditionnelle. 
 - Boucle bornée (for), boucle non bornée (while).

---

### Notion de fonction 

 - Fonctions à un ou plusieurs arguments. 
 - Fonction renvoyant un nombre aléatoire. Série statistique obtenue par la répétition de l’appel d’une telle fonction.

---

<cite>
À l’occasion de l’écriture d’algorithmes et de petits programmes, il convient de  transmettre 
aux  élèves  l’exigence  d’exactitude  et  de  rigueur,  et de les entraîner aux pratiques 
systématiques  de  vérification  et  de  contrôle.  En  programmant,  les  élèves  revisitent  les 
notions de variables et de fonctions sous une forme différente. 
</cite>